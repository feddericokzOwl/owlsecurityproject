package ar.org.osam.services.permissions.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"ar.org.osam.services.permissions"})
public class PermissionsServiceAutoConfiguration {
}
