package ar.org.osam.services.permissions;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import owl.feddericokz.system.services.PermissionsService;

@Component("permissionsService")
public class PermissionsServiceImpl implements PermissionsService {

    /*
     * Constants
     */

    private String PERMIT_ALL_PRIVILEGE = "ROLE_SYSTEM"; // todo This should come from properties.

    /*
     * Methods
     */

    @Override
    public boolean hasPermission(Authentication authentication, String sName) {
        return hasPermission(authentication,sName,"");
    }

    @Override
    public boolean hasPermission(Authentication authentication, String sName, String mName) {
        if (authentication.getAuthorities().contains(new SimpleGrantedAuthority(PERMIT_ALL_PRIVILEGE))) return true;
        return authentication.getAuthorities().contains(buildAuthority(sName,mName));
    }

    /*
     * Private Methods
     */

    private SimpleGrantedAuthority buildAuthority(String sName, String mName){
        return new SimpleGrantedAuthority("ROLE_"+sName.toUpperCase()+"_"+mName.toUpperCase());
    }

}
