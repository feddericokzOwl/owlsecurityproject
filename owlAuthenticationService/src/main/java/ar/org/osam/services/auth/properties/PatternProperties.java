package ar.org.osam.services.auth.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "network")
public class PatternProperties {

    private String arp;
    private String arp_a;
    private String requestHeader;
    private String localhostIpv6Address;
    private String regexMacLinux;
    private String regexWindows;
    private String regexEmail;

}
