package ar.org.osam.services.auth.dataholders;

import ar.org.osam.domains.auth.dataholders.NetworkRequestInfo;
import ar.org.osam.services.auth.properties.PatternProperties;
import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class NetworkRequestInfoFactory {

    /*
     * Services
     */

    @Autowired
    private NetworkUtils networkUtils;

    /*
     * Methods
     */

    public NetworkRequestInfo createFrom(HttpServletRequest request) {
        return networkUtils.getDeviceAddresses.apply(request);
    }

    /*
     * Inner Class
     */

    @Component
    public static class NetworkUtils {

        /*
         * Services
         */

        @Autowired
        private PatternProperties patternProperties;

        /*
         * Methods
         */

        public Function<NetworkRequestInfo,NetworkRequestInfo> getLocalHostAddress = networkRequestInfo -> {
            try {
                Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
                while (networkInterfaces.hasMoreElements()) {
                    NetworkInterface networkInterface = networkInterfaces.nextElement();
                    if (networkInterface.isLoopback() || !networkInterface.isUp() || networkInterface.isVirtual() || networkInterface.isPointToPoint())
                        continue;
                    Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
                    while (inetAddresses.hasMoreElements()) {
                        InetAddress inetAddress = inetAddresses.nextElement();
                        if (inetAddress.getClass().equals(Inet4Address.class))
                            networkRequestInfo.setIpAddress(inetAddress.getHostAddress());
                        if (networkRequestInfo.getMacAddress()==null) {
                            byte[] macAddress = networkInterface.getHardwareAddress();
                            StringBuilder stringBuilder = new StringBuilder();
                            for (int i = 0; i < macAddress.length; i++) {
                                stringBuilder.append(String.format("%02X%s", macAddress[i], (i < macAddress.length - 1) ? "-" : ""));
                            }
                            networkRequestInfo.setMacAddress(stringBuilder.toString());
                        }
                    }
                }
                return networkRequestInfo;
            } catch (SocketException e) {
                e.printStackTrace();
                throw new RuntimeException("Something went really wrong.");
            }
        };

        public Function<String,String> getClientMACAddress = ipAddress -> {
            Pattern macPattern = null;
            String OS = System.getProperty("os.name").toLowerCase();
            String[] command;
            if (OS.contains("win")) {
                macPattern = Pattern.compile(patternProperties.getRegexWindows());
                command = new String[]{patternProperties.getArp(), patternProperties.getArp_a(), ipAddress};
            } else {
                macPattern = Pattern.compile(patternProperties.getRegexMacLinux());
                command = new String[]{patternProperties.getArp(),ipAddress};
            }
            BufferedReader bufferedReader = null;
            try {
                Process process = Runtime.getRuntime().exec(command);
                process.waitFor();
                bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String line = bufferedReader.readLine();
                while (line!=null) {
                    Matcher matcher = macPattern.matcher(line);
                    if (matcher.find()) {
                        return matcher.group();
                    }
                    line=bufferedReader.readLine();
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (bufferedReader!=null) {
                        bufferedReader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new RuntimeException("Shit went wrong..");
                }
            }
            return "";
        };

        public Function<HttpServletRequest,NetworkRequestInfo> getDeviceAddresses = httpServletRequest -> {
            Assert.notNull(httpServletRequest,"Request cant be null, wrong!");
            NetworkRequestInfo networkRequestInfo = new NetworkRequestInfo();
            String remoteAdress = httpServletRequest.getHeader(patternProperties.getRequestHeader());
            if (remoteAdress == null || remoteAdress.isEmpty()) {
                remoteAdress = httpServletRequest.getRemoteAddr();
            }
            if (remoteAdress.equals(patternProperties.getLocalhostIpv6Address())) {
                getLocalHostAddress.apply(networkRequestInfo);
            } else {
                networkRequestInfo.setIpAddress(remoteAdress);
                networkRequestInfo.setMacAddress(getClientMACAddress.apply(remoteAdress));
            }
            return networkRequestInfo;
        };

    }

}
