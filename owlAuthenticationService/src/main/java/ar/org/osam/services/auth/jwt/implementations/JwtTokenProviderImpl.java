package ar.org.osam.services.auth.jwt.implementations;

import ar.org.osam.domains.auth.dataholders.JwtValues;
import ar.org.osam.domains.auth.dataholders.NetworkRequestInfo;
import ar.org.osam.services.auth.dataholders.NetworkRequestInfoFactory;
import ar.org.osam.services.auth.exceptions.UnauthorizedException;
import ar.org.osam.services.auth.jwt.JwtTokenProvider;
import ar.org.osam.services.auth.jwt.RSAKeyProvider;
import ar.org.osam.services.auth.properties.ErrorProperties;
import ar.org.osam.services.auth.properties.JwtProperties;
import ar.org.osam.services.auth.properties.SecurityProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.security.Key;
import java.util.Calendar;
import java.util.Date;

@Component
public class JwtTokenProviderImpl implements JwtTokenProvider {

    /*
     * Services
     */

    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JwtProperties jwtProperties;
    @Autowired
    private SecurityProperties securityProperties;
    @Autowired
    private ErrorProperties errorProperties;
    @Autowired
    private NetworkRequestInfoFactory requestInfoFactory;
    @Autowired
    private RSAKeyProvider keyProvider;

    /*
     * Init
     */

    @PostConstruct
    private void setUp() {
        //secretKey = Base64.getEncoder().encodeToString(jwtProperties.getSecretKey().getBytes());
    }

    /*
     * Methods
     */

    @Override
    public JwtValues getJwtValues() {
        return new JwtValues(securityProperties.getAuthorizationHeader(),securityProperties.getBearerPrefix());
    }

    @Override
    public Key getCurrentPublicKey() {
        return keyProvider.getPublic();
    }

    @Override
    public boolean validateToken(String token) {
        try {
            Jws<Claims> claimsJws = Jwts.parser()
                    .setSigningKey(keyProvider.getPublic())
                    .parseClaimsJws(token);
            return !claimsJws.getBody().getExpiration().before(Calendar.getInstance().getTime());
        } catch (IllegalArgumentException | JwtException e) {
            throw new UnauthorizedException(errorProperties.getInvalidTokenMessage(),errorProperties.getInvalidTokenDevMessage());
        }
    }

    @Override
    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails,"",userDetails.getAuthorities()); // todo Hmm, credentials? Should be empty?
    }

    @Override
    public String createToken(String username, HttpServletRequest request) {
        NetworkRequestInfo networkRequestInfo = requestInfoFactory.createFrom(request);
        return Jwts.builder()
                .setSubject(username)
                .claim("macAdress",networkRequestInfo.getMacAddress())
                .claim("ipAdress",networkRequestInfo.getIpAddress())
                .setIssuer(jwtProperties.getJwtKey())
                .setExpiration(calculateExpirationDate())
                .signWith(keyProvider.getPrivate())
                .compact();
    }

    /*
     * Private Methods
     */

    private String getUsername(String token) {
        return Jwts.parser().setSigningKey(keyProvider.getPublic()).parseClaimsJws(token).getBody().getSubject();
    }

    private Date calculateExpirationDate() {
        Date thisMoment = Calendar.getInstance().getTime();
        return new Date(thisMoment.getTime()+jwtProperties.getValidityInMilliseconds());
    }

}
