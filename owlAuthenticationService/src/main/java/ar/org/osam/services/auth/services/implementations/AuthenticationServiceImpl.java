package ar.org.osam.services.auth.services.implementations;

import ar.org.osam.services.auth.jwt.JwtTokenProvider;
import ar.org.osam.services.auth.services.AuthenticationService;
import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    /*
     * Services
     */

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    /*
     * Methods
     */

    @Override
    public Authentication authenticate(String token) {
        Assert.notNull(token,"The token was null.");
        return jwtTokenProvider.getAuthentication(token);
    }

}
