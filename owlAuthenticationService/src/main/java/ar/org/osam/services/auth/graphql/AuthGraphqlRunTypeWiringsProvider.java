package ar.org.osam.services.auth.graphql;

import ar.org.osam.domains.auth.PrivilegeDto;
import ar.org.osam.domains.auth.RoleDto;
import ar.org.osam.domains.auth.UserAccountDto;
import ar.org.osam.services.auth.services.PrivilegesService;
import ar.org.osam.services.auth.services.RolesService;
import ar.org.osam.services.auth.services.UserAccountsService;
import graphql.schema.idl.TypeRuntimeWiring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import owl.feddericokz.system.graphql.EntityByIdDataFetcher;
import owl.feddericokz.system.graphql.GraphqlRunTimeWiringProvider;

import java.util.ArrayList;
import java.util.List;

import static graphql.schema.idl.TypeRuntimeWiring.newTypeWiring;


@Component
public class AuthGraphqlRunTypeWiringsProvider implements GraphqlRunTimeWiringProvider {

    /*
     * Services
     */

    @Autowired
    private RolesService rolesService;
    @Autowired
    private PrivilegesService privilegesService;
    @Autowired
    private UserAccountsService userAccountsService;

    /*
     * Methods
     */

    @Override
    public List<TypeRuntimeWiring> getTypeRuntimeWirings() {
        List<TypeRuntimeWiring> runtimeWiringList = new ArrayList<>();
        runtimeWiringList.add(newTypeWiring("Query")
                .dataFetcher("roleById",new EntityByIdDataFetcher<RoleDto>(rolesService)).build());
        runtimeWiringList.add(newTypeWiring("Query")
                .dataFetcher("privilegeById",new EntityByIdDataFetcher<PrivilegeDto>(privilegesService)).build());
        runtimeWiringList.add(newTypeWiring("Query")
                .dataFetcher("userAccountById",new EntityByIdDataFetcher<UserAccountDto>(userAccountsService)).build());
        return runtimeWiringList;
    }

}
