package ar.org.osam.services.auth.services.implementations;

import ar.org.osam.domains.auth.Privilege;
import ar.org.osam.domains.auth.PrivilegeDto;
import ar.org.osam.services.auth.services.PrivilegesService;
import org.springframework.stereotype.Service;
import owl.feddericokz.system.services.support.EntityServiceSupport;

@Service
public class PrivilegesServiceImpl extends EntityServiceSupport<Privilege, Long> implements PrivilegesService {

    /*
     * Methods
     */

    @Override
    protected PrivilegeDto getDTOInstance() {
        return new PrivilegeDto();
    }

}
