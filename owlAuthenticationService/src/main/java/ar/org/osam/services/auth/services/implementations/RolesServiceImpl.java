package ar.org.osam.services.auth.services.implementations;

import ar.org.osam.domains.auth.Role;
import ar.org.osam.domains.auth.RoleDto;
import ar.org.osam.services.auth.services.RolesService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import owl.feddericokz.system.services.support.EntityServiceSupport;

@Service
public class RolesServiceImpl extends EntityServiceSupport<Role, Long> implements RolesService {

    /*
     * Methods
     */

    @Override
    protected RoleDto getDTOInstance() {
        return new RoleDto();
    }

}
