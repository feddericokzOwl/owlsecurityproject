package ar.org.osam.services.auth.services.implementations;

import ar.org.osam.domains.auth.*;
import ar.org.osam.services.auth.repositories.UserAccountsRepository;
import ar.org.osam.services.auth.services.UserAccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import owl.feddericokz.system.exceptions.EntityNotFoundException;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
public class OsmUserDetailsServiceImpl implements UserDetailsService {

    /*
     * Services
     */

    @Autowired
    private UserAccountsService userAccountsService;
    @Autowired
    private UserAccountsRepository userAccountsRepository;

    /*
     * Methods
     */

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserAccountDto userAccount = new UserAccountDto();
        Optional<UserAccount> userAccountOptional = userAccountsRepository.findOne((Specification<UserAccount>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(UserAccount_.USERNAME),username));
        if (userAccountOptional.isPresent()) {
            userAccount.fromDomain(userAccountOptional.get());
        } else {
            throw new UsernameNotFoundException("Username: "+username+" wasnt found.");
        }
        return new User(
                userAccount.getUsername(),
                userAccount.getPassword(),
                userAccount.getActivada(),
                userAccount.getFecha_caducidad() == null || userAccount.getFecha_caducidad().before(Calendar.getInstance().getTime()),
                userAccount.getFecha_caducidad_password() == null || userAccount.getFecha_caducidad_password().before(Calendar.getInstance().getTime()),
                !userAccount.getBloqueada(),
                getAuthorities(userAccount)
        );
    }

    private Collection<? extends GrantedAuthority> getAuthorities(UserAccountDto userAccount) {
        return getGrantedAuthorities(getPrivileges(userAccount));
    }

    private List<String> getPrivileges(UserAccountDto userAccount) {
        Collection<RoleDto> roles = userAccount.getRoles();
        List<String> privileges = userAccount.getPlusPrivileges().stream().map(PrivilegeDto::getDenominacion).collect(Collectors.toList());
        List<PrivilegeDto> collection = new ArrayList<>();
        for (RoleDto role : roles) {
            collection.addAll(role.getPrivileges());
        }
        for (PrivilegeDto item : collection) {
            privileges.add(item.getDenominacion());
        }
        return privileges;
    }

    private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }

}
