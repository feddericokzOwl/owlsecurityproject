package ar.org.osam.services.auth.repositories;

import ar.org.osam.domains.auth.Role;
import org.springframework.stereotype.Repository;
import owl.feddericokz.system.repositories.EntityRepository;

@Repository
public interface RolesRepository extends EntityRepository<Role, Long> {
}
