package ar.org.osam.services.auth.repositories;

import ar.org.osam.domains.auth.Privilege;
import org.springframework.stereotype.Repository;
import owl.feddericokz.system.repositories.EntityRepository;

@Repository
public interface PrivilegesRepository extends EntityRepository<Privilege, Long> {
}
