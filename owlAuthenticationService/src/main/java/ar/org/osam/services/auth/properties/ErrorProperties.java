package ar.org.osam.services.auth.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "error")
public class ErrorProperties {

    private String invalidTokenDevMessage;
    private String invalidTokenMessage;
    private String expiredAccountMessage;
    private String expiredAccountDevMessage;
    private String blockedAccountMessage;
    private String blockedAccountDevMessage;
    private String inactiveAccountDevMessage;
    private String inactiveAccountMessage;
    private String incorrectCredentialsMessage;
    private String incorrectCredentialsDevMessage;

}
