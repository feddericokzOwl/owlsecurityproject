package ar.org.osam.services.auth.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@ComponentScan(basePackages = {
        "ar.org.osam.services.auth.services.implementations",
        "ar.org.osam.services.auth.graphql",
        "ar.org.osam.services.auth.jwt",
        "ar.org.osam.services.auth.properties",
        "ar.org.osam.services.auth.dataholders"
})
public class AuthServicesAutoConfiguration {

    /*
     * Beans Configuration
     */

    @Bean
    @ConditionalOnMissingBean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
