package ar.org.osam.services.auth.jwt;

import ar.org.osam.domains.auth.dataholders.JwtValues;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import java.security.Key;

public interface JwtTokenProvider {

    /*
     * Methods
     */

    /*String resolveToken(HttpServletRequest request);*/

    JwtValues getJwtValues();

    Key getCurrentPublicKey();

    boolean validateToken(String token);

    Authentication getAuthentication(String token);

    String createToken(String username, HttpServletRequest request);

}
