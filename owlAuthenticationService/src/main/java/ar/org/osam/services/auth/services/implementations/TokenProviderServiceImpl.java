package ar.org.osam.services.auth.services.implementations;

import ar.org.osam.domains.auth.UserAccount;
import ar.org.osam.domains.auth.UserAccountDto;
import ar.org.osam.domains.auth.UserAccount_;
import ar.org.osam.domains.auth.dataholders.AuthCredentials;
import ar.org.osam.services.auth.exceptions.UnauthorizedException;
import ar.org.osam.services.auth.jwt.implementations.JwtTokenProviderImpl;
import ar.org.osam.services.auth.properties.ErrorProperties;
import ar.org.osam.services.auth.services.TokenProviderService;
import ar.org.osam.services.auth.services.UserAccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import owl.feddericokz.system.exceptions.EntityNotFoundException;

import javax.servlet.http.HttpServletRequest;
import java.security.Key;
import java.util.Calendar;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

@Service
public class TokenProviderServiceImpl implements TokenProviderService {

    /*
     * Services
     */

    @Autowired
    private UserAccountsService userAccountsService;
    @Autowired
    private JwtTokenProviderImpl jwtTokenProvider;
    @Autowired
    private ErrorProperties errorProperties;

    /*
     * Methods
     */

    @Override
    public String validateCredentials(AuthCredentials authCredentials, HttpServletRequest httpServletRequest) {
        UserAccountDto userAccount = fetchUserAccount.apply(authCredentials);
        validateAccount.accept(userAccount);
        validatePassword.accept(authCredentials,userAccount);
        return jwtTokenProvider.createToken(authCredentials.getUsername(),httpServletRequest);
    }

    @Override
    public Key getServerPublicKey() {
        return jwtTokenProvider.getCurrentPublicKey();
    }

    /*
     * Private Methods
     */

    private Function<AuthCredentials, UserAccountDto> fetchUserAccount = authRequest -> {
        try {
            return userAccountsService.findOne((Specification<UserAccount>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(UserAccount_.USERNAME),authRequest.getUsername()));
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    };

    private Consumer<UserAccountDto> validateAccount = userAccount -> {
        if (userAccount!=null) {
            if (userAccount.getFecha_caducidad()!=null && !userAccount.getFecha_caducidad().before(Calendar.getInstance().getTime())) {
                throw new UnauthorizedException(errorProperties.getExpiredAccountMessage(),errorProperties.getExpiredAccountDevMessage());
            }
            if (userAccount.getBloqueada()) {
                throw new UnauthorizedException(errorProperties.getBlockedAccountMessage(),errorProperties.getBlockedAccountDevMessage());
            }
            if (!userAccount.getActivada()) {
                throw new UnauthorizedException(errorProperties.getInactiveAccountMessage(),errorProperties.getInactiveAccountDevMessage());
            }
        } else {
            throw new RuntimeException("Server Error, UserAccount objects isn't supposed to be null");
        }
    };

    private BiConsumer<AuthCredentials, UserAccountDto> validatePassword = (authCredentials, userAccount) -> {
        if (!BCrypt.checkpw(authCredentials.getPassword(),userAccount.getPassword())) {
            throw new UnauthorizedException(errorProperties.getIncorrectCredentialsMessage(),errorProperties.getIncorrectCredentialsDevMessage());
        }
    };

}
