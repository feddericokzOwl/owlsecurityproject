package ar.org.osam.services.auth.repositories;

import ar.org.osam.domains.auth.UserAccount;
import org.springframework.stereotype.Repository;
import owl.feddericokz.system.repositories.EntityRepository;

@Repository
public interface UserAccountsRepository extends EntityRepository<UserAccount, Long> {
}
