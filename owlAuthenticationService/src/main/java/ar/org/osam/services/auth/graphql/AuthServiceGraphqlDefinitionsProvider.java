package ar.org.osam.services.auth.graphql;

import org.springframework.stereotype.Component;
import owl.feddericokz.system.graphql.support.GraphqlDefinitionsProviderSupport;

import javax.annotation.PostConstruct;

@Component
public class AuthServiceGraphqlDefinitionsProvider extends GraphqlDefinitionsProviderSupport {

    /*
     * Init
     */

    @PostConstruct
    private void setUp() {
        this.setClassLoader(this.getClass().getClassLoader());
        this.setSchemaPath("graphql/services/auth/schema.graphqls");
    }

}
