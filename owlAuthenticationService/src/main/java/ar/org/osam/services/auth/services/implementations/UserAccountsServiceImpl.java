package ar.org.osam.services.auth.services.implementations;

import ar.org.osam.domains.auth.*;
import ar.org.osam.domains.auth.dataholders.UserRegistration;
import ar.org.osam.services.auth.exceptions.UsernameInUseException;
import ar.org.osam.services.auth.services.RolesService;
import ar.org.osam.services.auth.services.UserAccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import owl.feddericokz.system.exceptions.EntityNotFoundException;
import owl.feddericokz.system.exceptions.InvalidEntityException;
import owl.feddericokz.system.exceptions.StaleResourceException;
import owl.feddericokz.system.services.support.EntityServiceSupport;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserAccountsServiceImpl extends EntityServiceSupport<UserAccount, Long> implements UserAccountsService {

    /*
     * Fields
     */

    private static final String BASIC_AUTHORITHY = "ROLE_USER";

    /*
     * Services
     */

    @Autowired
    private RolesService rolesService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    /*
     * Methods
     */

    @Override
    protected UserAccountDto getDTOInstance() {
        return new UserAccountDto();
    }

    @Override
    public UserAccountDto registerNewUserAccount(UserRegistration registration) throws UsernameInUseException {
        UserAccountDto newUser = new UserAccountDto();
        try {
            this.findOne((Specification<UserAccount>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(UserAccount_.USERNAME), newUser.getUsername()));
            throw new UsernameInUseException("Username: "+registration.getUsername()+" is already in use by another account.");
        } catch (EntityNotFoundException e) {
            try {
                // todo This is the place where the expiration dates and the other settings are set during registration.
                newUser.setUsername(registration.getUsername());
                newUser.setPassword(passwordEncoder.encode(registration.getPassword()));
                newUser.setRoles(Collections.singletonList(rolesService.findOne((Specification<Role>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(Role_.DENOMINACION), BASIC_AUTHORITHY))));
                return this.save(newUser);
            } catch (InvalidEntityException | StaleResourceException | EntityNotFoundException ex) {
                throw new RuntimeException(); // todo This can be less generic.
            }
        }
    }

    @Override
    public List<String> giveMeMyRoles(String username) throws EntityNotFoundException {
        UserAccountDto userAccount = this.findOne((Specification<UserAccount>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(UserAccount_.USERNAME),username));
        return userAccount.getRoles().stream().map(RoleDto::getDenominacion).collect(Collectors.toList());
    }

}
