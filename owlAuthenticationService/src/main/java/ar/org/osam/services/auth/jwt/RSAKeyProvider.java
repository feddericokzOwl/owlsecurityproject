package ar.org.osam.services.auth.jwt;

import java.security.Key;

public interface RSAKeyProvider {

    Key getPrivate();

    Key getPublic();

}
