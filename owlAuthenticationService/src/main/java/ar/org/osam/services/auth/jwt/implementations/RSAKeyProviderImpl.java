package ar.org.osam.services.auth.jwt.implementations;

import ar.org.osam.services.auth.jwt.RSAKeyProvider;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.security.KeyPair;

@Component
public class RSAKeyProviderImpl implements RSAKeyProvider {

    /*
     * Fields
     */

    KeyPair keyPair = Keys.keyPairFor(SignatureAlgorithm.RS256);

    /*
     * Methods
     */

    @Override
    public Key getPrivate() {
        return keyPair.getPrivate();
    }

    @Override
    public Key getPublic() {
        return keyPair.getPublic();
    }

}
