package ar.org.osam.services.auth.rest.clients;

import ar.org.osam.services.auth.rest.controllers.AuthenticationRestController;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "osmAuthenticationRestService", url = "${feign.client.auth.serverUrl}")
public interface AuthenticationRestClient extends AuthenticationRestController {
}
