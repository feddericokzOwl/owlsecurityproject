package ar.org.osam.services.auth.security;

import ar.org.osam.domains.auth.dataholders.AuthCredentials;
import ar.org.osam.services.auth.rest.clients.AuthenticationRestClient;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.bouncycastle.util.encoders.Base64;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import owl.feddericokz.rest.exceptions.BadHeaderException;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Calendar;
import java.util.Objects;

@Component
public class OwlJwtTokenVault {

    public OwlJwtTokenVault(AuthenticationRestClient authenticationRestClient) {
        this.authenticationRestClient = authenticationRestClient;
    }

    private AuthenticationRestClient authenticationRestClient;

    private static final String HEADER_KEY = "Public-Key";

    private String token;
    private PublicKey publicKey = null;
    private AuthCredentials credentials;

    public void setToken(String token){
        this.token= token;
    }

    public String getToken(){
        if(isExpired()){
            return getTokenFromService();
        }else{
            return token;
        }
    }

    public void setCredentials(AuthCredentials credentials) {
        this.credentials = credentials;
    }

    private boolean isExpired() {
        if(publicKey == null || token == null)
            return true;

        return Jwts.parser().setSigningKey(publicKey)
                .parseClaimsJws(token)
                .getBody().getExpiration().before(Calendar.getInstance().getTime());
    }

    private String getTokenFromService(){

        if (credentials != null) {
            String name = credentials.getUsername();
            String pass = credentials.getPassword();

            if(credentials.getUsername().equals(SecurityContextHolder.getContext().getAuthentication().getName())){
                throw new SecurityException("No Coincide el usuario");
            }

            ResponseEntity<String> response = authenticationRestClient.validateCredentials(new AuthCredentials(name, pass));
            if(response.getStatusCode() == HttpStatus.OK){


                if(response.getHeaders().containsKey(HEADER_KEY)){
                try{
                    KeyFactory rsa = KeyFactory.getInstance("RSA");
                    X509EncodedKeySpec key = new X509EncodedKeySpec(Base64.decode(Objects.requireNonNull(response.getHeaders().get(HEADER_KEY)).get(0)));
                    publicKey = rsa.generatePublic(key);

                }catch (Exception e){
                    e.printStackTrace();
                }
                }else{
                    throw new BadHeaderException("PublicKey not present");
                }


                Jws<Claims> claimsJws = Jwts.parser()
                        .setSigningKey(publicKey)
                        .parseClaimsJws(response.getBody());

                token = response.getBody();


            }else{
                throw new BadCredentialsException("Error");
            }
        }
        return token;
    }

}
