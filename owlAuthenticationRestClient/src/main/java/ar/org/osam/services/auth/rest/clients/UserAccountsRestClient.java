package ar.org.osam.services.auth.rest.clients;

import ar.org.osam.domains.auth.UserAccount;
import ar.org.osam.domains.auth.UserAccountDto;
import ar.org.osam.domains.auth.dataholders.UserRegistration;
import ar.org.osam.services.auth.exceptions.UsernameInUseException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import owl.feddericokz.rest.controllers.EntityRestController;
import owl.feddericokz.system.exceptions.EntityNotFoundException;

import java.util.List;

@FeignClient(value = "osmUserAccountsRestService", url = "${feign.client.auth.serverUrl}/api/resources/auth/accounts")
public interface UserAccountsRestClient extends EntityRestController<UserAccountDto, UserAccount,Long> {

    @PostMapping("/register")
    ResponseEntity<?> registerNewUserAccount(@RequestBody UserRegistration registration) throws UsernameInUseException;

    @PostMapping("/giveMeRoles")
    ResponseEntity<List<String>> giveMeMyRoles(@RequestBody String username) throws EntityNotFoundException;

}
