package ar.org.osam.services.auth.rest.clients.configuration;

import ar.org.osam.services.auth.rest.configuration.FeignConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = {
        "ar.org.osam.services.auth.rest.clients"
},
        defaultConfiguration = FeignConfiguration.class)
public class AuthRestClientsConfiguration {
}
