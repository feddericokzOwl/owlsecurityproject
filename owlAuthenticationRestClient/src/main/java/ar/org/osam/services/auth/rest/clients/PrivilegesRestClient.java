package ar.org.osam.services.auth.rest.clients;

import ar.org.osam.domains.auth.Privilege;
import ar.org.osam.domains.auth.PrivilegeDto;
import org.springframework.cloud.openfeign.FeignClient;
import owl.feddericokz.rest.controllers.EntityRestController;

@FeignClient(value = "osmPrivilegesRestService", url = "${feign.client.auth.serverUrl}/api/resources/auth/privileges")
public interface PrivilegesRestClient extends EntityRestController<PrivilegeDto, Privilege, Long> {
}
