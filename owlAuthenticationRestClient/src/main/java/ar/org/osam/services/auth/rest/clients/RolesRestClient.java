package ar.org.osam.services.auth.rest.clients;

import ar.org.osam.domains.auth.Role;
import ar.org.osam.domains.auth.RoleDto;
import org.springframework.cloud.openfeign.FeignClient;
import owl.feddericokz.rest.controllers.EntityRestController;

@FeignClient(value = "osmRolesRestService", url = "${feign.client.auth.serverUrl}/api/resources/auth/roles")
public interface RolesRestClient extends EntityRestController<RoleDto, Role,Long> {
}
