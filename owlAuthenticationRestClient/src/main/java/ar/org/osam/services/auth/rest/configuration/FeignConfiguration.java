package ar.org.osam.services.auth.rest.configuration;

import ar.org.osam.services.auth.security.AuthForwardInterceptor;
import ar.org.osam.services.auth.security.AuthSetTokenInterceptor;
import feign.okhttp.OkHttpClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfiguration {

    /*
     * Fields
     */

    final ApplicationContext context;

    /*
     * Constructors
     */

    public FeignConfiguration(ApplicationContext context) {
        this.context = context;
    }

    /*
     * Beans
     */

    @Bean
    public AuthForwardInterceptor authForwardInterceptor() {
        return new AuthForwardInterceptor();
    }

    @Bean
    public AuthSetTokenInterceptor authSetTokenInterceptor() {
        AuthSetTokenInterceptor authSetTokenInterceptor = new AuthSetTokenInterceptor();
        authSetTokenInterceptor.setApplicationContext(context);
        return authSetTokenInterceptor;
    }

    @Bean
    public OkHttpClient okHttpClient() {
        return new OkHttpClient();
    }

}
