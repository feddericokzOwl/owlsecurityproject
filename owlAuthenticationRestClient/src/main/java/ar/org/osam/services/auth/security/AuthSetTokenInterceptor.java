package ar.org.osam.services.auth.security;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class AuthSetTokenInterceptor implements RequestInterceptor, ApplicationContextAware {

    /*
     * Fields
     */

    private ApplicationContext context;

    /*
     * Methods
     */

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    @Override
    public void apply(RequestTemplate template) {
        ServletRequestAttributes servletRequestAttributes = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
        if (servletRequestAttributes==null && !template.url().contains("authentication/login")) {
            //HttpServletRequest servletRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            String token = context.getBean(OwlJwtTokenVault.class).getToken();

            template.header(HttpHeaders.AUTHORIZATION, "Bearer:" + token);
        }
    }
}
