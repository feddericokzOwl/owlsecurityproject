package ar.org.osam.services.auth.services;

import ar.org.osam.domains.auth.Role;
import org.springframework.security.access.prepost.PreAuthorize;
import owl.feddericokz.system.services.EntityService;

@PreAuthorize("@permissionsService.hasPermission(authentication,'rolesService')")
public interface RolesService extends EntityService<Role, Long> {
}
