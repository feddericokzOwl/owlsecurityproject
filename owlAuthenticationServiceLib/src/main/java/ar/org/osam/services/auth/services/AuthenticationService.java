package ar.org.osam.services.auth.services;

import org.springframework.security.core.Authentication;

public interface AuthenticationService {

    Authentication authenticate(String token);

}
