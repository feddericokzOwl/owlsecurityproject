package ar.org.osam.services.auth.services;

import ar.org.osam.domains.auth.Privilege;
import owl.feddericokz.system.services.EntityService;

public interface PrivilegesService extends EntityService<Privilege, Long> {
}
