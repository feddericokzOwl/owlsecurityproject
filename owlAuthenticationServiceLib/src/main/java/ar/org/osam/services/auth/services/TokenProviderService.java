package ar.org.osam.services.auth.services;

import ar.org.osam.domains.auth.dataholders.AuthCredentials;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.servlet.http.HttpServletRequest;
import java.security.Key;

public interface TokenProviderService {

    String validateCredentials(AuthCredentials authCredentials, HttpServletRequest httpServletRequest);

    @PreAuthorize("hasRole('ROLE_USER')")
    Key getServerPublicKey();

}
