package ar.org.osam.services.auth.services;

import ar.org.osam.domains.auth.UserAccount;
import ar.org.osam.domains.auth.UserAccountDto;
import ar.org.osam.domains.auth.dataholders.UserRegistration;
import ar.org.osam.services.auth.exceptions.UsernameInUseException;
import org.springframework.security.access.prepost.PreAuthorize;
import owl.feddericokz.system.exceptions.EntityNotFoundException;
import owl.feddericokz.system.services.EntityService;

import java.util.List;

public interface UserAccountsService extends EntityService<UserAccount, Long> {

    UserAccountDto registerNewUserAccount(UserRegistration registration) throws UsernameInUseException;

    @PreAuthorize("#username == principal.username")
    List<String> giveMeMyRoles(String username) throws EntityNotFoundException;

}
