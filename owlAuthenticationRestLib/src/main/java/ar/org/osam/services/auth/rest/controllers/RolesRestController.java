package ar.org.osam.services.auth.rest.controllers;

import ar.org.osam.domains.auth.Role;
import ar.org.osam.domains.auth.RoleDto;
import org.springframework.web.bind.annotation.RequestMapping;
import owl.feddericokz.rest.controllers.EntityRestController;

@RequestMapping("/api/resources/auth/roles")
public interface RolesRestController extends EntityRestController<RoleDto, Role,Long> {
}
