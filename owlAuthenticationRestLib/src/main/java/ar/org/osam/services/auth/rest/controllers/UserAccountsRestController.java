package ar.org.osam.services.auth.rest.controllers;

import ar.org.osam.domains.auth.UserAccount;
import ar.org.osam.domains.auth.UserAccountDto;
import ar.org.osam.domains.auth.dataholders.UserRegistration;
import ar.org.osam.services.auth.exceptions.UsernameInUseException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import owl.feddericokz.rest.controllers.EntityRestController;
import owl.feddericokz.system.exceptions.EntityNotFoundException;

import java.util.List;

@RequestMapping("/api/resources/auth/accounts")
public interface UserAccountsRestController extends EntityRestController<UserAccountDto, UserAccount,Long> {

    @PostMapping("/register")
    ResponseEntity<?> registerNewUserAccount(@RequestBody UserRegistration registration) throws UsernameInUseException;

    @PostMapping("/giveMeRoles")
    ResponseEntity<List<String>> giveMeMyRoles(@RequestBody String username) throws EntityNotFoundException;

}
