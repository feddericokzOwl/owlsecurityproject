package ar.org.osam.services.auth.rest.controllers;

import ar.org.osam.domains.auth.Privilege;
import ar.org.osam.domains.auth.PrivilegeDto;
import org.springframework.web.bind.annotation.RequestMapping;
import owl.feddericokz.rest.controllers.EntityRestController;

@RequestMapping("/api/resources/auth/privileges")
public interface PrivilegesRestController extends EntityRestController<PrivilegeDto, Privilege, Long> {
}
