package ar.org.osam.services.auth.rest.controllers;

import ar.org.osam.domains.auth.dataholders.AuthCredentials;
import ar.org.osam.domains.auth.dataholders.JwtValues;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/authentication")
public interface AuthenticationRestController {

    @PostMapping("/login")
    ResponseEntity<String> validateCredentials(@RequestBody AuthCredentials authCredentials);

    @GetMapping("/getPublicKey")
    ResponseEntity<String> getServerPublicKey();

    @PostMapping("/authenticate")
    ResponseEntity<UsernamePasswordAuthenticationToken> authenticate(@RequestBody String token);

    @GetMapping("/getJwtValues")
    ResponseEntity<JwtValues> getJwtValues();

    @PostMapping("/validateToken")
    ResponseEntity<Boolean> validateToken(@RequestBody String token);

}
