package ar.org.osam.domains.auth;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Privilege.class)
public abstract class Privilege_ extends owl.feddericokz.system.domains.support.ServiceReadySupport_ {

	public static volatile SingularAttribute<Privilege, String> denominacion;

	public static final String DENOMINACION = "denominacion";

}

