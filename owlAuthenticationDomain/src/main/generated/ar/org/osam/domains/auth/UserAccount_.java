package ar.org.osam.domains.auth;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserAccount.class)
public abstract class UserAccount_ extends owl.feddericokz.system.domains.support.ServiceReadySupport_ {

	public static volatile SingularAttribute<UserAccount, String> password;
	public static volatile SingularAttribute<UserAccount, Boolean> activada;
	public static volatile SingularAttribute<UserAccount, Date> fecha_caducidad_password;
	public static volatile SingularAttribute<UserAccount, Boolean> tokenExpired;
	public static volatile CollectionAttribute<UserAccount, Role> roles;
	public static volatile SingularAttribute<UserAccount, Boolean> bloqueada;
	public static volatile CollectionAttribute<UserAccount, Privilege> plusPrivileges;
	public static volatile SingularAttribute<UserAccount, String> username;
	public static volatile SingularAttribute<UserAccount, Date> fecha_caducidad;

	public static final String PASSWORD = "password";
	public static final String ACTIVADA = "activada";
	public static final String FECHA_CADUCIDAD_PASSWORD = "fecha_caducidad_password";
	public static final String TOKEN_EXPIRED = "tokenExpired";
	public static final String ROLES = "roles";
	public static final String BLOQUEADA = "bloqueada";
	public static final String PLUS_PRIVILEGES = "plusPrivileges";
	public static final String USERNAME = "username";
	public static final String FECHA_CADUCIDAD = "fecha_caducidad";

}

