package ar.org.osam.domains.auth;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Role.class)
public abstract class Role_ extends owl.feddericokz.system.domains.support.ServiceReadySupport_ {

	public static volatile CollectionAttribute<Role, Privilege> privileges;
	public static volatile SingularAttribute<Role, String> denominacion;

	public static final String PRIVILEGES = "privileges";
	public static final String DENOMINACION = "denominacion";

}

