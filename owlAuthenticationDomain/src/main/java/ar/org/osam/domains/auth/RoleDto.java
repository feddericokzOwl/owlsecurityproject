package ar.org.osam.domains.auth;

import lombok.*;
import owl.feddericokz.system.domains.EntityDto;
import owl.feddericokz.system.domains.ServiceDto;
import owl.feddericokz.system.utils.ServiceEntityReflectiveMerger;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class RoleDto implements EntityDto<Role, Long>, Serializable {

    /*
     * Fields
     */

    private Long id;
    private LocalDateTime versionTag;
    private String denominacion;
    private Collection<PrivilegeDto> privileges = new ArrayList<>();

    /*
     * Constructors
     */

    public RoleDto(Role attachedRole) {
        populateObjects(attachedRole);
    }

    /*
     * Methods
     */

    @Override
    public ServiceDto fromDomain(Role attachedRole) {
        populateObjects(attachedRole);
        return this;
    }

    @Override
    public Role toDomain() {
        if (this.getId()!=null) {
            try {
                Role attachedRole = new Role();
                ServiceEntityReflectiveMerger.mergeDto(attachedRole,this);
                if (!attachedRole.autoValidate()) return null;
                return attachedRole;
            } catch (IllegalArgumentException e) {
                return null;
            }
        } else {
            Role attachedRole = new Role();
            attachedRole.packageSetId(this.getId());
            attachedRole.packageSetVersionTag(this.getVersionTag());
            ServiceEntityReflectiveMerger.mergeDto(attachedRole,this);
            attachedRole.packageSetPrivilegesCollection(this.getPrivileges().stream().map(PrivilegeDto::toDomain).collect(Collectors.toList()));
            if (!attachedRole.autoValidate()) return null;
            return attachedRole;
        }
    }

    private void populateObjects(Role attachedRole) {
        if (attachedRole!=null) {
            this.id = attachedRole.getId();
            this.versionTag = attachedRole.getVersionTag();
            this.denominacion = attachedRole.getDenominacion();
            this.privileges = attachedRole.getPrivileges().stream().map(PrivilegeDto::new).collect(Collectors.toList());
        }
    }

}
