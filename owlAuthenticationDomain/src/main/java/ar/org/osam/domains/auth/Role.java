package ar.org.osam.domains.auth;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;
import owl.feddericokz.system.domains.support.ServiceReadySupport;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
@Table(name = "osm_auth_roles")
@EqualsAndHashCode(callSuper = true)
@Setter(value = AccessLevel.PRIVATE) // All setters are private except those declared public.
@NoArgsConstructor(access = AccessLevel.PACKAGE) // This constructor is usable from the same package, , it's intented to be used from the DTO's
public class Role extends ServiceReadySupport<Long> {

    /*
     * Fields
     */

    private String denominacion;
    private Collection<Privilege> privileges;

    /*
     * Getters
     */

    @Column(name = "denominacion")
    public String getDenominacion() {
        return denominacion;
    }

    @ManyToMany
    @JoinTable(
            name = "osm_rel_auth_roles_privileges",
            joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = {@JoinColumn(name = "privilege_id")}
    )
    public Collection<Privilege> getPrivileges() {
        return privileges;
    }

    /*
     * Setters
     */

    void packageSetId(Long id) {
        this.setId(id);
    }

    void packageSetVersionTag(LocalDateTime versionTag) {
        this.setVersionTag(versionTag);
    }

    void packageSetPrivilegesCollection(Collection<Privilege> privileges) {
        this.setPrivileges(privileges);
    }

    /*
     * Methods
     */

    @Override
    public boolean autoValidate() {
        return denominacion!=null;
    }

}
