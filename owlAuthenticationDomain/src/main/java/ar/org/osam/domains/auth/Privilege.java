package ar.org.osam.domains.auth;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;
import owl.feddericokz.system.domains.support.ServiceReadySupport;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "osm_auth_privileges")
@EqualsAndHashCode(callSuper = true)
@Setter(value = AccessLevel.PRIVATE) // All setters are private except those declared public.
@NoArgsConstructor(access = AccessLevel.PACKAGE) // This constructor is usable from the same package, , it's intented to be used from the DTO's
public class Privilege extends ServiceReadySupport<Long> {

    /*
     * Fields
     */

    private String denominacion;

    /*
     * Getters
     */

    @Column(name = "denominacion")
    public String getDenominacion() {
        return denominacion;
    }

    /*
     * Setters
     * These methods have package access level so they can be used to build objects from the DTO's
     */

    void packageSetId(Long id) {
        this.setId(id);
    }

    void packageSetVersionTag(LocalDateTime versionTag) {
        this.setVersionTag(versionTag);
    }

    /*
     * Methods
     */

    @Override
    public boolean autoValidate() {
        return denominacion!=null;
    }

}
