package ar.org.osam.domains.auth.dataholders;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class NetworkRequestInfo implements Serializable {

    /*
     * Fields
     */

    private String ipAddress;
    private String macAddress;

}
