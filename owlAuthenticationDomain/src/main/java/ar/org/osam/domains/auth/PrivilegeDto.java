package ar.org.osam.domains.auth;

import lombok.*;
import owl.feddericokz.system.domains.EntityDto;
import owl.feddericokz.system.domains.ServiceDto;
import owl.feddericokz.system.utils.ServiceEntityReflectiveMerger;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class PrivilegeDto implements EntityDto<Privilege, Long>, Serializable {

    /*
     * Fields
     */

    private Long id;
    private LocalDateTime versionTag;
    private String denominacion;

    /*
     * Constructors
     */

    public PrivilegeDto(Privilege attachedPrivilege) {
        populateObjects(attachedPrivilege);
    }

    /*
     * Methods
     */

    @Override
    public ServiceDto fromDomain(Privilege attachedPrivilege) {
        populateObjects(attachedPrivilege);
        return this;
    }

    @Override
    public Privilege toDomain() {
        if (this.getId()!=null) {
            try {
                Privilege attachedPrivilege = new Privilege();
                ServiceEntityReflectiveMerger.mergeDto(attachedPrivilege,this);
                if (!attachedPrivilege.autoValidate()) return null;
                return attachedPrivilege;
            } catch (IllegalArgumentException e) {
                return null;
            }
        } else {
            Privilege attachedPrivilege = new Privilege();
            attachedPrivilege.packageSetId(this.getId());
            attachedPrivilege.packageSetVersionTag(this.versionTag);
            ServiceEntityReflectiveMerger.mergeDto(attachedPrivilege,this);
            if (!attachedPrivilege.autoValidate()) return null;
            return attachedPrivilege;
        }
    }

    private void populateObjects(Privilege attachedPrivilege) {
        if (attachedPrivilege!=null) {
            this.id = attachedPrivilege.getId();
            this.versionTag = attachedPrivilege.getVersionTag();
            this.denominacion = attachedPrivilege.getDenominacion();
        }
    }

}
