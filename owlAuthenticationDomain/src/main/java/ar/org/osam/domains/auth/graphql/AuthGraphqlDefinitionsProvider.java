package ar.org.osam.domains.auth.graphql;

import org.springframework.stereotype.Component;
import owl.feddericokz.system.graphql.support.GraphqlDefinitionsProviderSupport;

import javax.annotation.PostConstruct;

@Component
public class AuthGraphqlDefinitionsProvider extends GraphqlDefinitionsProviderSupport {

    /*
     * Init
     */

    @PostConstruct
    private void setUp() {
        this.setClassLoader(this.getClass().getClassLoader());
        this.setSchemaPath("graphql/domains/auth/schema.graphqls");
    }

}
