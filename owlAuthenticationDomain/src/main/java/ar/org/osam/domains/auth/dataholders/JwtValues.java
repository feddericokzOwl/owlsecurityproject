package ar.org.osam.domains.auth.dataholders;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class JwtValues implements Serializable {

    private String authHeader;
    private String bearerPrefix;

}
