package ar.org.osam.domains.auth.dataholders;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthCredentials implements Serializable {

    /*
     * Fields
     */

    private String username;
    private String password;

}
