package ar.org.osam.domains.auth;

import lombok.*;
import owl.feddericokz.system.domains.EntityDto;
import owl.feddericokz.system.domains.ServiceDto;
import owl.feddericokz.system.utils.ServiceEntityReflectiveMerger;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class UserAccountDto implements EntityDto<UserAccount, Long>, Serializable {

    /*
     * Fields
     */

    private Long id;
    private LocalDateTime versionTag;
    private String password;
    private String username;
    private Date fecha_caducidad;
    private Date fecha_caducidad_password;
    private Boolean bloqueada;
    private Boolean activada;
    private Boolean tokenExpired;
    private Collection<RoleDto> roles = new ArrayList<>();
    private Collection<PrivilegeDto> plusPrivileges = new ArrayList<>();

    /*
     * Constructors
     */

    public UserAccountDto(UserAccount attachedUserAccount) {
        populateObjects(attachedUserAccount);
    }

    /*
     * Methods
     */

    @Override
    public ServiceDto fromDomain(UserAccount attachedUserAccount) {
        populateObjects(attachedUserAccount);
        return this;
    }

    @Override
    public UserAccount toDomain() {
        if (this.getId()!=null) {
            try {
                UserAccount attachedUserAccount = new UserAccount();
                ServiceEntityReflectiveMerger.mergeDto(attachedUserAccount,this);
                if (!attachedUserAccount.autoValidate()) return null;
                return attachedUserAccount;
            } catch (IllegalArgumentException e) {
                return null;
            }
        } else {
            UserAccount attachedUserAccount = new UserAccount();
            attachedUserAccount.packageSetId(this.getId());
            attachedUserAccount.packageSetVersionTag(this.getVersionTag());
            ServiceEntityReflectiveMerger.mergeDto(attachedUserAccount,this);
            attachedUserAccount.packageSetPrivilegesCollection(this.getPlusPrivileges().stream().map(PrivilegeDto::toDomain).collect(Collectors.toList()));
            attachedUserAccount.packageSetRolesCollection(this.getRoles().stream().map(RoleDto::toDomain).collect(Collectors.toList()));
            if (!attachedUserAccount.autoValidate()) return null;
            return attachedUserAccount;
        }
    }

    private void populateObjects(UserAccount attachedUserAccount) {
        if (attachedUserAccount!=null) {
            this.id = attachedUserAccount.getId();
            this.versionTag = attachedUserAccount.getVersionTag();
            this.password = attachedUserAccount.getPassword();
            this.username = attachedUserAccount.getUsername();
            this.fecha_caducidad = attachedUserAccount.getFecha_caducidad();
            this.fecha_caducidad_password = attachedUserAccount.getFecha_caducidad_password();
            this.bloqueada = attachedUserAccount.getBloqueada();
            this.activada = attachedUserAccount.getActivada();
            this.tokenExpired = attachedUserAccount.getTokenExpired();
            this.roles = attachedUserAccount.getRoles().stream().map(RoleDto::new).collect(Collectors.toList());
            this.plusPrivileges = attachedUserAccount.getPlusPrivileges().stream().map(PrivilegeDto::new).collect(Collectors.toList());
        }
    }

}
