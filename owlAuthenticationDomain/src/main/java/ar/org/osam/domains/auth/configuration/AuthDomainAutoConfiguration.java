package ar.org.osam.domains.auth.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EntityScan(basePackages = "ar.org.osam.domains.auth")
@ComponentScan(basePackages = "ar.org.osam.domains.auth.graphql")
public class AuthDomainAutoConfiguration {
}
