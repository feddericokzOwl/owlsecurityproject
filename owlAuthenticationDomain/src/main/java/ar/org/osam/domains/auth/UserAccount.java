package ar.org.osam.domains.auth;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;
import owl.feddericokz.system.domains.support.ServiceReadySupport;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "osm_auth_user_account")
@EqualsAndHashCode(callSuper = true)
@Setter(value = AccessLevel.PRIVATE) // All setters are private except those declared public.
@NoArgsConstructor(access = AccessLevel.PACKAGE)// This constructor is usable from the same package, , it's intented to be used from the DTO's
public class UserAccount extends ServiceReadySupport<Long> {

    /*
     * Fields
     */

    private String password;
    private String username;
    private Date fecha_caducidad;
    private Date fecha_caducidad_password;
    private Boolean bloqueada;
    private Boolean activada;
    private Boolean tokenExpired;
    private Collection<Role> roles;
    private Collection<Privilege> plusPrivileges;

    /*
     * Getters
     */

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    @Column(name = "fecha_caducidad")
    public Date getFecha_caducidad() {
        return fecha_caducidad;
    }

    @Column(name = "fecha_caducidad_password")
    public Date getFecha_caducidad_password() {
        return fecha_caducidad_password;
    }

    @Column(name = "bloqueada")
    public Boolean getBloqueada() {
        return bloqueada;
    }

    @Column(name = "activada")
    public Boolean getActivada() {
        return activada;
    }

    @Column(name = "token_expired")
    public Boolean getTokenExpired() {
        return tokenExpired;
    }

    @ManyToMany
    @JoinTable(
            name = "osm_rel_auth_role_user",
            joinColumns = {@JoinColumn(name = "user_account_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")}
    )
    public Collection<Role> getRoles() {
        return roles;
    }

    @ManyToMany
    @JoinTable(
            name = "osm_rel_auth_priv_user",
            joinColumns = {@JoinColumn(name = "user_account_id")},
            inverseJoinColumns = {@JoinColumn(name = "privilege_id")}
    )
    public Collection<Privilege> getPlusPrivileges() {
        return plusPrivileges;
    }

    /*
     * Setters
     */

    void packageSetId(Long id) {
        this.setId(id);
    }

    void packageSetVersionTag(LocalDateTime versionTag) {
        this.setVersionTag(versionTag);
    }

    void packageSetRolesCollection(Collection<Role> roles) {
        this.setRoles(roles);
    }

    void packageSetPrivilegesCollection(Collection<Privilege> privileges) {
        this.setPlusPrivileges(privileges);
    }

    /*
     * Methods
     */

    @Override
    public boolean autoValidate() {
        boolean isValid = true;
        if (password==null) isValid=false;
        if (username==null) isValid=false;
        return isValid;
    }

}
