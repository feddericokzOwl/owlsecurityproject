package ar.org.osam.services.auth.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;


@Data
@EqualsAndHashCode(callSuper = true)
public class UnauthorizedException extends RuntimeException {

    /*
     * Fields
     */

    private ErrorResponse errorResponse;

    /*
     * Constructors
     */

    public UnauthorizedException(String msg, String devMsg) {
        this.errorResponse = new ErrorResponse(HttpStatus.UNAUTHORIZED, msg, devMsg);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ErrorResponse {

        /*
         * Fields
         */

        private HttpStatus status;
        private String errorMsg;
        private String developerMsg;

    }

}
