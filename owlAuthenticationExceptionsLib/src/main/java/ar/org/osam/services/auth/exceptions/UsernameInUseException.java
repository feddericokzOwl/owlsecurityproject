package ar.org.osam.services.auth.exceptions;

public class UsernameInUseException extends Exception {

    public UsernameInUseException() {
    }

    public UsernameInUseException(String message) {
        super(message);
    }

}
