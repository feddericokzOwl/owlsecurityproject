-- CREATE TABLE osm_auth_privileges (
--     id INT AUTO_INCREMENT PRIMARY KEY,
--     denominacion VARCHAR NOT NULL,
--     last_updated TIMESTAMP NOT NULL
-- );
--
-- CREATE TABLE osm_auth_roles (
--     id INT AUTO_INCREMENT PRIMARY KEY,
--     denominacion VARCHAR NOT NULL,
--     last_updated TIMESTAMP NOT NULL
-- );
--
-- CREATE TABLE osm_rel_auth_roles_privileges (
--     id INT AUTO_INCREMENT PRIMARY KEY,
--     role_id INT NOT NULL,
--     privilege_id INT NOT NULL,
--     foreign key (role_id) references osm_auth_roles(id),
--     foreign key (privilege_id) references osm_auth_privileges(id)
-- );
--
-- CREATE TABLE osm_auth_user_account (
--     id INT AUTO_INCREMENT PRIMARY KEY,
--     last_updated TIMESTAMP NOT NULL,
--     password VARCHAR NOT NULL,
--     username VARCHAR NOT NULL,
--     fecha_caducidad DATE,
--     fecha_caducidad_password DATE,
--     bloqueada BOOLEAN,
--     activada BOOLEAN,
--     token_expired BOOLEAN
-- );
--
-- CREATE TABLE osm_rel_auth_role_user (
--     id INT AUTO_INCREMENT PRIMARY KEY ,
--     user_account_id INT NOT NULL,
--     role_id INT NOT NULL,
--     foreign key (user_account_id) references osm_auth_user_account(id),
--     foreign key (role_id) references osm_auth_roles(id)
-- );
--
-- CREATE TABLE osm_rel_auth_priv_user (
--     id INT AUTO_INCREMENT PRIMARY KEY,
--     user_account_id INT NOT NULL,
--     privilege_id INT NOT NULL,
--     foreign key (user_account_id) references osm_auth_user_account(id),
--     foreign key (privilege_id) references osm_auth_privileges(id)
-- );

INSERT INTO osm_auth_user_account(last_updated, password, username, bloqueada, activada, token_expired)
    VALUES (now(),'$2a$10$9hkfBG6cLwvUA24ZdoGQjuNFfXT1x4/MlnLq15QxLWK25xpRmYeVu','feddericokz',false,true,false);
INSERT INTO osm_auth_privileges(denominacion, last_updated)
    VALUES ('ROLE_SYSTEM',now());
INSERT INTO osm_rel_auth_priv_user(user_account_id, privilege_id)
    VALUES (1,1);