package ar.org.osam.services.auth.rest.controllers.implementations;

import ar.org.osam.domains.auth.Role;
import ar.org.osam.domains.auth.RoleDto;
import ar.org.osam.services.auth.rest.controllers.RolesRestController;
import org.springframework.web.bind.annotation.RestController;
import owl.feddericokz.rest.controllers.support.EntityRestControllerSupport;

@RestController
public class RolesRestControllerImpl extends EntityRestControllerSupport<RoleDto, Role,Long> implements RolesRestController {
}
