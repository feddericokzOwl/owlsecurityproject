package ar.org.osam.services.auth.rest.controllers.implementations;

import ar.org.osam.domains.auth.dataholders.AuthCredentials;
import ar.org.osam.domains.auth.dataholders.JwtValues;
import ar.org.osam.services.auth.jwt.JwtTokenProvider;
import ar.org.osam.services.auth.rest.controllers.AuthenticationRestController;
import ar.org.osam.services.auth.services.AuthenticationService;
import ar.org.osam.services.auth.services.TokenProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.List;

@RestController
public class AuthenticationRestControllerImpl implements AuthenticationRestController {

    /*
     * Services
     */

    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private TokenProviderService tokenProviderService;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    /*
     * Methods
     */

    @Override
    public ResponseEntity<String> validateCredentials(AuthCredentials authCredentials) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        this.levelUpPrivileges(SecurityContextHolder.getContext().getAuthentication(),"ROLE_USERACCOUNTSSERVICE_FINDONE");
        return ResponseEntity.ok()
                .header("Public-Key", Base64.getEncoder().encodeToString(jwtTokenProvider.getCurrentPublicKey().getEncoded()))
                .body(tokenProviderService.validateCredentials(authCredentials,request));
    }

    @Override
    public ResponseEntity<String> getServerPublicKey() {
        return ResponseEntity.ok(Base64.getEncoder().encodeToString(tokenProviderService.getServerPublicKey().getEncoded()));
    }

    @Override
    public ResponseEntity<UsernamePasswordAuthenticationToken> authenticate(String token) {
        return ResponseEntity.ok((UsernamePasswordAuthenticationToken) authenticationService.authenticate(fixBrokenToken(token)));
    }

    @Override
    public ResponseEntity<JwtValues> getJwtValues() {
        return ResponseEntity.ok(jwtTokenProvider.getJwtValues());
    }

    @Override
    public ResponseEntity<Boolean> validateToken(String token) {
        return ResponseEntity.ok(jwtTokenProvider.validateToken(fixBrokenToken(token)));
    }

    /*
     * Private Methods
     */

    private String fixBrokenToken(String token) {
        if (token.startsWith("\"")) token = token.substring(1);
        if (token.endsWith("\"")) token = token.substring(0,token.length()-1);
        return token;
    }

    private void levelUpPrivileges(Authentication authentication, String privilege) {
        /*
         * Kids, dont do this at home.
         */
        Collection<GrantedAuthority> privilegesList = new ArrayList<>(authentication.getAuthorities());
        privilegesList.add(new SimpleGrantedAuthority(privilege));
        Class authClazz = authentication.getClass();
        Field privilegesCollectionField;
        if (authentication instanceof AnonymousAuthenticationToken) {
            try {
                privilegesCollectionField = authClazz.getSuperclass().getDeclaredField("authorities");
                privilegesCollectionField.setAccessible(true);
                privilegesCollectionField.set(authentication,privilegesList);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

}
