package ar.org.osam.services.auth.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import owl.feddericokz.rest.controllers.graphql.configuration.EnableOwlGraphQL;
import owl.feddericokz.rest.controllers.owl.configuration.EnableOwlWatch;
import owl.feddericokz.system.services.configuration.EnableOwlSecurity;

@SpringBootApplication
@EnableDiscoveryClient
@EnableOwlSecurity
@EnableOwlGraphQL
@EnableOwlWatch
public class AuthenticationRestServiceContextConfiguration {

    public static void main(String args[]){
        SpringApplication.run(AuthenticationRestServiceContextConfiguration.class, args);
    }

}
