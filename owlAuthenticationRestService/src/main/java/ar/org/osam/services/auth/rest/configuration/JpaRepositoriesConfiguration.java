package ar.org.osam.services.auth.rest.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = {
        "ar.org.osam.services.auth.repositories"
})
public class JpaRepositoriesConfiguration {
}
