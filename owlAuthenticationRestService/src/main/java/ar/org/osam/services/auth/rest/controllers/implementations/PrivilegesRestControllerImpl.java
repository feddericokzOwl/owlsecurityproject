package ar.org.osam.services.auth.rest.controllers.implementations;

import ar.org.osam.domains.auth.Privilege;
import ar.org.osam.domains.auth.PrivilegeDto;
import ar.org.osam.services.auth.rest.controllers.PrivilegesRestController;
import org.springframework.web.bind.annotation.RestController;
import owl.feddericokz.rest.controllers.support.EntityRestControllerSupport;

@RestController
public class PrivilegesRestControllerImpl extends EntityRestControllerSupport<PrivilegeDto, Privilege,Long> implements PrivilegesRestController {
}
