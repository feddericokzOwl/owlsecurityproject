package ar.org.osam.services.auth.rest.controllers.implementations;

import ar.org.osam.domains.auth.UserAccount;
import ar.org.osam.domains.auth.UserAccountDto;
import ar.org.osam.domains.auth.dataholders.UserRegistration;
import ar.org.osam.services.auth.exceptions.UsernameInUseException;
import ar.org.osam.services.auth.rest.controllers.UserAccountsRestController;
import ar.org.osam.services.auth.services.UserAccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import owl.feddericokz.rest.controllers.support.EntityRestControllerSupport;
import owl.feddericokz.system.exceptions.EntityNotFoundException;

import java.util.List;

@RestController
public class UserAccountsRestControllerImpl extends EntityRestControllerSupport<UserAccountDto, UserAccount,Long> implements UserAccountsRestController {

    /*
     * Services
     */

    @Autowired
    private UserAccountsService userAccountsService;

    /*
     * Methods
     */

    @Override
    public ResponseEntity<?> registerNewUserAccount(UserRegistration registration) throws UsernameInUseException {
        return ResponseEntity.ok(userAccountsService.registerNewUserAccount(registration)!=null);
    }


    @Override
    public ResponseEntity<List<String>> giveMeMyRoles(String username) throws EntityNotFoundException {
        return ResponseEntity.ok(userAccountsService.giveMeMyRoles(username));
    }

}
