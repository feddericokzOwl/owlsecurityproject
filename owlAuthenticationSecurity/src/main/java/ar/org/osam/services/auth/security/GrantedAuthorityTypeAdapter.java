package ar.org.osam.services.auth.security;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.io.IOException;

public class GrantedAuthorityTypeAdapter extends TypeAdapter<GrantedAuthority> {

    @Override
    public void write(JsonWriter out, GrantedAuthority value) throws IOException {
        new Gson().getAdapter(SimpleGrantedAuthority.class).write(out, (SimpleGrantedAuthority) value);
        // todo Im pretty sure this isnt working.
    }

    @Override
    public GrantedAuthority read(JsonReader in) throws IOException {
        SimpleGrantedAuthority simpleGrantedAuthority = null;
        in.beginObject();
        String fieldName = null;
        while (in.hasNext()) {
            JsonToken jsonToken = in.peek();
            if (jsonToken.equals(JsonToken.NAME)) {
                fieldName=in.nextName();
            }
            if (fieldName!=null && fieldName.equals("authority")) {
                jsonToken = in.peek();
                simpleGrantedAuthority = new SimpleGrantedAuthority(in.nextString());
            }
        }
        in.endObject();
        return simpleGrantedAuthority;
    }

}
