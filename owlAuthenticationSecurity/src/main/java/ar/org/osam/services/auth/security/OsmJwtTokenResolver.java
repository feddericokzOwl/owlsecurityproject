package ar.org.osam.services.auth.security;

import ar.org.osam.domains.auth.dataholders.JwtValues;
import ar.org.osam.services.auth.rest.controllers.AuthenticationRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class OsmJwtTokenResolver {

    /*
     * Fields
     */

    private JwtValues jwtValues = null;

    /*
     * Services
     */

    @Autowired
    private AuthenticationRestController authenticationRestClient;

    /*
     * Methods
     */

    public String resolveToken(HttpServletRequest request) {
        if (jwtValues == null) jwtValues = authenticationRestClient.getJwtValues().getBody();
        assert jwtValues != null;
        String bearerToken = request.getHeader(jwtValues.getAuthHeader());
        return (bearerToken!=null && bearerToken.startsWith(jwtValues.getBearerPrefix())) ?
                bearerToken.substring(jwtValues.getBearerPrefix().length()+1).trim() : null;
    }

}
