package ar.org.osam.services.auth.security;

import ar.org.osam.services.auth.rest.controllers.AuthenticationRestController;
import com.google.gson.Gson;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class OsmJwtTokenFilter extends OncePerRequestFilter {

    /*
     * Fields
     */

    //private JwtTokenProvider jwtTokenProvider;
    private AuthenticationRestController authenticationRestClient;
    private OsmJwtTokenResolver jwtTokenResolver;
    private Gson gson;

    /*
     * Constructors
     */

    /*public OsmJwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }*/

    public OsmJwtTokenFilter(AuthenticationRestController authenticationRestClient, OsmJwtTokenResolver jwtTokenResolver) {
        this.authenticationRestClient = authenticationRestClient;
        this.jwtTokenResolver = jwtTokenResolver;
    }

    /*
     * Methods
     */

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = jwtTokenResolver.resolveToken(request);
        if (token!=null && authenticationRestClient.validateToken(token).getBody()) {
            UsernamePasswordAuthenticationToken authentication = authenticationRestClient.authenticate(token).getBody();
            Assert.notNull(authentication,"Authentication came back null, ultra error!");
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        filterChain.doFilter(request,response);
    }

}
