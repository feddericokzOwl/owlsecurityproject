package ar.org.osam.services.auth.security;

import ar.org.osam.services.auth.rest.controllers.AuthenticationRestController;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class OsmJwtConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    /*
     * Fields
     */

    //private JwtTokenProvider jwtTokenProvider;

    private AuthenticationRestController authenticationRestClient;
    private OwlJwtTokenResolver jwtTokenResolver;

    /*
     * Constructors
     */

    /*public OsmJwtConfigurer(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }*/

    public OsmJwtConfigurer(AuthenticationRestController authenticationRestClient, OwlJwtTokenResolver jwtTokenResolver) {
        this.authenticationRestClient = authenticationRestClient;
        this.jwtTokenResolver = jwtTokenResolver;
    }

    /*
     * Methods
     */

    @Override
    public void configure(HttpSecurity builder) throws Exception {
        OwlJwtTokenFilter jwtTokenFilter = new OwlJwtTokenFilter(authenticationRestClient, jwtTokenResolver);
        builder.exceptionHandling()
                .authenticationEntryPoint(new OwlJwtAuthenticationEntryPoint())
                .and()
                .addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class);
    }

}
