package ar.org.osam.services.auth.security.configuration;

import ar.org.osam.services.auth.security.GrantedAuthorityTypeAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;

@Configuration
@ComponentScan(basePackages = {
        "ar.org.osam.services.auth.security"
})
public class AuthSecurityAutoConfiguration {

    /*
     * Beans
     */


    @Bean
    public Gson gson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(GrantedAuthority.class, new GrantedAuthorityTypeAdapter());
        return gsonBuilder.create();
    }

}
